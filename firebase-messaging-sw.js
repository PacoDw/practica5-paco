//importScripts('/__/firebase/4.10.0/firebase-app.js');
//importScripts('/__/firebase/4.10.0/firebase-messaging.js');
//importScripts('/__/firebase/init.js');

//var messaging = firebase.messaging();


// * Here is is the code snippet to initialize Firebase Messaging in the Service
//* Worker when your app is not hosted on Firebase Hosting.
// [START initialize_firebase_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
/*firebase.initializeApp({
  'messagingSenderId': 'YOUR-SENDER-ID'
});*/
var config = {
  apiKey: "AIzaSyCQMa3tsHul3M6JCKwWh2kYhCrO20BqrPY",
  authDomain: "clase-mantenimiento-fb3ec.firebaseapp.com",
  databaseURL: "https://clase-mantenimiento-fb3ec.firebaseio.com",
  projectId: "clase-mantenimiento-fb3ec",
  storageBucket: "",
  messagingSenderId: "335184879377"
};

firebase.initializeApp(config);
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
//const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]
//**/
var messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});