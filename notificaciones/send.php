<? php
    define('SERVER_API_KEY', 'AIzaSyBXGOIUQmbdj60vHdf5GGEms_2WF2-3988');
    $tokens = ['IJHI3_DA2K9:APA91bGr5Rq-Gz7Iu5Dv_2S-qSbAygNItpovW8I33zfIsuEvNRlsB7nxfA16E7d5bXTONC3k_VkmOLlF1ckWYODB26g0oid9KGAV9QRXVDeIQqTe-BwBzyT6wsnhf7X9gM_NKrCLRvUx', 'caMTj6dtBg8:APA91bGeCMcXNjq5s-lMLlpY5l08Kh0tIb78nb333o0gcYaZC4rAGtKPYYHdQCIrlEh3Nma2eazhWVmF-3rOtqvXFqZyb6eL6IFtDa9NF-zvsRfzV4fLFDfWIHHQ6gCCbmeo2wcdxnrR'];
    $header = [
        'Authorization: key='.SERVER_API_KEY,
        'Content-Type: Application/json'
    ];
    $msg = [
        'title' => 'Se acabó el semestre',
        'body' => 'Espero hayas aprendido cosas nuevas en esta materia',
        'icon' => 'img/educ-app.png',
        'img' => 'img/image.png',
    ];
    $payload = array(
        'registration_ids' => $tokens,
        'data' => $msg
    );
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($payload),
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HTTPHEADER => $header
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "URL Error #:" . $err;
    } else {
        echo $response;
    }
?>