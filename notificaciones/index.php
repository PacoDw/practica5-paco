<!DOCTYPE html>
<html>

<head>
  <title>Aplicacion con notificaciones</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
  <script>
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyCQMa3tsHul3M6JCKwWh2kYhCrO20BqrPY",
      authDomain: "clase-mantenimiento-fb3ec.firebaseapp.com",
      databaseURL: "https://clase-mantenimiento-fb3ec.firebaseio.com",
      projectId: "clase-mantenimiento-fb3ec",
      storageBucket: "",
      messagingSenderId: "335184879377"
    };
    firebase.initializeApp(config);
  </script>

  <script>
    const messaging = firebase.messaging();

    function saveToken(currentToken) {
      $.ajax({
        url: 'action.php',
        method: 'post',
        data: 'token=' + currentToken
      }).done(function (result) {
        console.log(currentToken);
      })
    }


    function setTokenSentToServer(sent) {
      window.localStorage.setItem('sentToServer', sent ? 1 : 0);
    }

    function isTokenSentToServer() {
      return window.localStorage.getItem('sentToServer') == 1;
    }

    function getRegToken() {
      messaging.getToken().then(function (currentToken) {
        if (currentToken) {
          console.log(currentToken);
          saveToken(currentToken);
          setTokenSentToServer(true)
        } else {
          console.log('No Instance ID token available. Request permission to generate one.');
          setTokenSentToServer(false);
        }
      }).catch(function (err) {
        console.log('An error occurred while retrieving token. ', err);
        setTokenSentToServer(false);
      });
    }

    messaging.requestPermission().then(function () {
      console.log('Notification permission granted.');
      if (isTokenSentToServer()) {
        console.log('token ya fue enviado');
      } else {
        //recuperarToken();
        getRegToken();
      }
    }).catch(function (err) {
      console.log('Unable to get permission to notify.', err);
    });
  </script>

  <script>
    messaging.onMessage(function (payload) {
      var title = payload.data.title;
      var options = {
        body: payload.data.body,
        icon: payload.data.icon
      }
      var myNotification = new Notification(title, options);
      console.log('Mensaje recibidio', payload)
    })
  </script>
  <link rel="manifest" href="manifest.json">
</head>

<body>
  <h1> App con notificaciones </h1>
</body>

</html>